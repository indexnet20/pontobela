<!DOCTYPE html>
<html lang="pt-br">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/style.css">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Lora:ital,wght@1,700&family=Nunito:ital,wght@0,200;0,300;0,500;0,600;0,700;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<title><?php bloginfo('name'); ?> - <?php bloginfo('description'); ?></title>
</head>

<body <?php body_class(); ?> id="topo">

	<header>
		<div class="container d-flex">
			<div class="logo">
				<a href="/pontobela">
					<img src="<?= get_template_directory_uri(); ?>/img/ponto.svg" alt="Logo Ponto Bela">
				</a>
			</div>
			<div>
				<?php
				wp_nav_menu(array(
					'menu'   => 'Header',
				));
				?>
			</div>
			<div class="search">
				<?php echo get_search_form(); ?>
			</div>
			<div class="align-items-center justify-content-center" id="open-sidebar">
				<button onclick="openSidebar()">
					<img src="<?= get_template_directory_uri(); ?>/img/bars-solid.svg" alt="Abrir Menu">
				</button>
			</div>
		</div>
	</header>

	<sidebar>
		<div class="logo">
			<a href="/pontobela">
				<img src="<?= get_template_directory_uri(); ?>/img/ponto.svg" alt="Logo Ponto Bela">
			</a>
		</div>
		<div>
			<?php
			wp_nav_menu(array(
				'menu'   => 'Header',
			));
			?>
		</div>
	</sidebar>

	<div class="overlay"></div>