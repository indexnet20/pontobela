<?php get_header();

$the_query =  get_posts(array('posts_per_page'  => 2, 'offset' => 0,));
$the_query_2 =  get_posts(array('posts_per_page'  => 3, 'offset' => 0,  'category' => 11));
$the_query_3 =  get_posts(array('posts_per_page'  => 3, 'offset' => 3,  'category' => 11));
$the_query_lidos =  get_posts(array('posts_per_page'  => 2, 'offset' => 0, 'category' => 11));
$the_query_lidos_1 =  get_posts(array('posts_per_page'  => 2, 'offset' => 2, 'category' => 11));
$the_query_destaques =  get_posts(array('posts_per_page'  => 2, 'offset' => 0, 'category' => 0));
?>



<div class="container d-flex w-100 h-100 align-items-center pt-4 flex-column single-post">
  <h1 class="content-post-principal"><?php echo get_the_title(); ?></h1>
  <div class="content-post">
    <div>
      <?php while (have_posts()) : the_post();  ?>
        <div>
          <h2><img src="<?= get_template_directory_uri(); ?>/img/monograma-pack-sem-fundo-ponto-bela-bullet.svg" alt="Icon Ponto"><? echo get_the_category()[0]->name; ?></h2>
        </div>
        <a href="<?php echo get_permalink($the_query[0]->ID); ?>">
          <img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($the_query[0]->post_title) ?>">
        </a>

        <?php
        the_content();
        ?>


        <?php $postUrl = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"; ?>

        <div class="share-button-wrapper">
          <a target="_blank" class="share-button share-twitter" href="https://twitter.com/intent/tweet?url=<?php echo $postUrl; ?>&text=<?php echo the_title(); ?>&via=<?php the_author_meta('twitter'); ?>" title="Tweet this">
            <img src="<?= get_template_directory_uri(); ?>/img/twitter-brands.svg" alt="">
          </a>
          <a target="_blank" class="share-button share-facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $postUrl; ?>" title="Share on Facebook">
            <img src="<?= get_template_directory_uri(); ?>/img/facebook-brands.svg" alt="">
          </a>
          <a target="_blank" class="share-button share-facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $postUrl; ?>" title="Share on Facebook">
            <img src="<?= get_template_directory_uri(); ?>/img/instagram-brands.svg" alt="">
          </a>
        </div>

      <?php endwhile; ?>
    </div>
    <div>
      <div class="post-destaques">
        <h2><img src="<?= get_template_directory_uri(); ?>/img/monograma-pack-sem-fundo-ponto-bela-bullet.svg" alt="Icon Ponto">Destaques</h2>
        <?php foreach ($the_query_destaques as $ret) { ?>
          <div>
            <a href="<?php echo get_permalink($ret->ID); ?>">
              <img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($ret->post_title) ?>">

            </a>
            <a href="<?php echo get_permalink($ret->ID); ?>">
              <p><?php print_r($ret->post_title) ?></p>
            </a>
            <div><?php print_r($ret->post_content) ?></div>
          </div>
        <?php } ?>
      </div>
      <div class="post-lidas">
        <h2><img src="<?= get_template_directory_uri(); ?>/img/monograma-pack-sem-fundo-ponto-bela-bullet.svg" alt="Icon Ponto">Mais Lidas</h2>
        <?php foreach ($the_query_2 as $ret) { ?>
          <div>
            <a href="<?php echo get_permalink($ret->ID); ?>">
              <p><?php print_r($ret->post_title) ?></p>
            </a>
            <a href="<?php echo get_permalink($ret->ID); ?>">
              <img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($ret->post_title) ?>">
            </a>
          </div>
        <?php } ?>
      </div>
      <div class="post-propaganda">

      </div>
      <div class="post-lidas">
        <?php foreach ($the_query_3 as $ret) { ?>
          <div>
            <a href="<?php echo get_permalink($ret->ID); ?>">
              <p><?php print_r($ret->post_title) ?></p>
            </a>
            <a href="<?php echo get_permalink($ret->ID); ?>">
              <img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($ret->post_title) ?>">
            </a>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>


  <div class="container-most-wanted">
    <h2><img src="<?= get_template_directory_uri(); ?>/img/monograma-pack-sem-fundo-ponto-bela-bullet.svg" alt="Icon Ponto">Mais Buscados Hoje</h2>
    <div class="most-wanted-main">
      <div>
        <?php foreach ($the_query_lidos as $ret) { ?>
          <div>
            <a href="<?php echo get_permalink($ret->ID); ?>">
              <img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($ret->post_title) ?>">
            </a>
            <a href="<?php echo get_permalink($ret->ID); ?>">
              <p><?php print_r($ret->post_title) ?></p>
            </a>
            <div><?php print_r($ret->post_content) ?></div>

            <a href="<?php echo get_permalink($ret->ID); ?>">Leia Mais</a>
          </div>
        <?php } ?>
      </div>

      <div>
        <?php foreach ($the_query_lidos_1 as $ret) { ?>
          <div>
            <a href="<?php echo get_permalink($ret->ID); ?>">
              <img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($ret->post_title) ?>">
            </a>
            <a href="<?php echo get_permalink($ret->ID); ?>">
              <p><?php print_r($ret->post_title) ?></p>
            </a>
            <div><?php print_r($ret->post_content) ?></div>

            <a href="<?php echo get_permalink($ret->ID); ?>">Leia Mais</a>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>


  <a href='#topo'>Voltar ao topo</a>
</div>
<?php get_footer(); ?>