<?php
// Template name: Home
get_header();
$urlTheme = get_template_directory_uri();
?>

<?php
$the_query =  get_posts(array('posts_per_page'  => 10, 'offset' => 0,));
$the_query_four =  get_posts(array('posts_per_page'  => 4, 'offset' => 3,));

$the_query_six =  get_posts(array('posts_per_page'  => 1, 'offset' => 0, 'category' => 4));
$the_query_six_2 =  get_posts(array('posts_per_page'  => 6, 'offset' => 1, 'category' => 4));

$the_query_alisamento_1 =  get_posts(array('posts_per_page'  => 1, 'offset' => 0, 'category' => 2));
$the_query_alisamento =  get_posts(array('posts_per_page'  => 6, 'offset' => 1, 'category' => 2));

$the_query_cabelos_1 =  get_posts(array('posts_per_page'  => 1, 'offset' => 0, 'category' => 5));
$the_query_cabelos =  get_posts(array('posts_per_page'  => 6, 'offset' => 1, 'category' => 5));

$the_query_dicas_3 =  get_posts(array('posts_per_page'  => 5, 'offset' => 6, 'category' => 3));

$the_query_dicas_1 =  get_posts(array('posts_per_page'  => 1, 'offset' => 0, 'category' => 3));
$the_query_dicas =  get_posts(array('posts_per_page'  => 6, 'offset' => 1, 'category' => 3));

$the_query_negocios_1 =  get_posts(array('posts_per_page'  => 1, 'offset' => 0, 'category' => 4));
$the_query_negocios =  get_posts(array('posts_per_page'  => 6, 'offset' => 1, 'category' => 4));

$the_query_lidos =  get_posts(array('posts_per_page'  => 2, 'offset' => 0, 'category' => 11));
$the_query_lidos_1 =  get_posts(array('posts_per_page'  => 2, 'offset' => 2, 'category' => 11));
?>

<div class="container d-flex w-100 h-100 align-items-center pt-4 flex-column home">
  <h1>Seu ponto de referência no mundo dos cosméticos capilares</h1>
  <div class="linha"></div>
  <p>Abril - 2022</p>

  <div class="content-main">
    <div>
      <a href="<?php echo get_permalink($the_query[0]->ID); ?>">
        <img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($the_query[0]->post_title) ?>">
      </a>
      <a href="<?php echo get_permalink($the_query[0]->ID); ?>">
        <p><?php print_r($the_query[0]->post_title) ?></p>
      </a>
      <div><?php print_r($the_query[0]->post_content) ?></div>
      <a href="<?php echo get_permalink($ret->ID); ?>">Leia Mais</a>
    </div>
    <div>
      <div class="container-duo">
        <div>
          <a href="<?php echo get_permalink($the_query[1]->ID); ?>">
            <img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($the_query[1]->post_title) ?>">
          </a>
          <a href="<?php echo get_permalink($the_query[1]->ID); ?>">
            <p><?php print_r($the_query[1]->post_title) ?></p>
          </a>
          <div><?php print_r($the_query[1]->post_content) ?></div>
        </div>

        <div>
          <a href="<?php echo get_permalink($the_query[2]->ID); ?>">
            <img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($the_query[2]->post_title) ?>">
          </a>
          <a href="<?php echo get_permalink($the_query[2]->ID); ?>">
            <p><?php print_r($the_query[2]->post_title) ?></p>
          </a>
          <div><?php print_r($the_query[2]->post_content) ?></div>
        </div>
      </div>

      <div class="container-three">
        <?php foreach ($the_query_four as $ret) { ?>
          <div>
            <a href="<?php echo get_permalink($ret->ID); ?>">
              <img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($ret->post_title) ?>">

            </a>
            <a href="<?php echo get_permalink($ret->ID); ?>">
              <p><?php print_r($ret->post_title) ?></p>
            </a>
          </div>
        <?php } ?>
      </div>
    </div>

  </div>

  <div class="container-most-wanted">
    <h2><img src="<?= get_template_directory_uri(); ?>/img/monograma-pack-sem-fundo-ponto-bela-bullet.svg" alt="Icon Ponto">Mais Buscados Hoje</h2>
    <div class="most-wanted-main">
      <div>
        <?php foreach ($the_query_lidos as $ret) { ?>
          <div>
            <a href="<?php echo get_permalink($ret->ID); ?>">
              <img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($ret->post_title) ?>">
            </a>
            <a href="<?php echo get_permalink($ret->ID); ?>">
              <p><?php print_r($ret->post_title) ?></p>
            </a>
            <div><?php print_r($ret->post_content) ?></div>

            <a href="<?php echo get_permalink($ret->ID); ?>">Leia Mais</a>
          </div>
        <?php } ?>
      </div>

      <div>
        <?php foreach ($the_query_lidos_1 as $ret) { ?>
          <div>
            <a href="<?php echo get_permalink($ret->ID); ?>">
              <img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($ret->post_title) ?>">
            </a>
            <a href="<?php echo get_permalink($ret->ID); ?>">
              <p><?php print_r($ret->post_title) ?></p>
            </a>
            <div><?php print_r($ret->post_content) ?></div>

            <a href="<?php echo get_permalink($ret->ID); ?>">Leia Mais</a>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>

  <div class="container-dicas">
    <h2>Super Dicas</h2>
    <div>
      <?php foreach ($the_query_dicas_3 as $ret) { ?>
        <div>
          <a href="<?php echo get_permalink($ret->ID); ?>">
            <img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($ret->post_title) ?>">

          </a>
          <a href="<?php echo get_permalink($ret->ID); ?>">
            <p><?php print_r($ret->post_title) ?></p>
          </a>
        </div>
      <?php } ?>
    </div>
    <a href="category/dicas">
      <img src="<?= get_template_directory_uri(); ?>/img/add_circle_outline_black_48dp.svg" alt="Botão ver Mais">
    </a>
  </div>

  <div class="container-business">
    <h2><img src="<?= get_template_directory_uri(); ?>/img/monograma-pack-sem-fundo-ponto-bela-bullet.svg" alt="Icon Ponto">Négocios</h2>
    <div class="bussines-content">
      <div>
        <a href="<?php echo get_permalink($the_query_six[0]->ID); ?>">
          <img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($the_query[0]->post_title) ?>">
        </a>
        <a href="<?php echo get_permalink($the_query_six[0]->ID); ?>">
          <p><?php print_r($the_query_six[0]->post_title) ?></p>
        </a>
        <div><?php print_r($the_query_six[0]->post_content) ?></div>

        <a href="<?php echo get_permalink($the_query_six[0]->ID); ?>">Leia Mais</a>
      </div>
      <div>
        <div>
          <?php foreach ($the_query_six_2 as $ret) { ?>
            <div>
              <a href="<?php echo get_permalink($ret->ID); ?>">
                <img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($ret->post_title) ?>">

              </a>
              <a href="<?php echo get_permalink($ret->ID); ?>">
                <p><?php print_r($ret->post_title) ?></p>
              </a>
            </div>
          <?php } ?>
        </div>
        <div></div>
      </div>
    </div>
  </div>

  <div class="container-categories">
    <div class="categories-alisamento">
      <div>
        <p>Alisamento</p>
        <a href="<?php echo get_permalink($the_query_alisamento_1[0]->ID); ?>">
          <img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($the_query_alisamento_1[0]->post_title) ?>">
        </a>
        <a href="<?php echo get_permalink($the_query_alisamento_1[0]->ID); ?>">
          <p><?php print_r($the_query_alisamento_1[0]->post_title) ?></p>
        </a>
        <div><?php print_r($the_query_alisamento_1[0]->post_content) ?></div>
        <a href="<?php echo get_permalink($the_query_alisamento_1[0]->ID); ?>">Leia Mais</a>
      </div>
      <?php foreach ($the_query_alisamento as $ret) { ?>
        <div>
          <a href="<?php echo get_permalink($ret->ID); ?>">
            <p><?php print_r($ret->post_title) ?></p>
          </a>
        </div>
      <?php } ?>
    </div>
    <div class="categories-cabelos">
      <div>
        <p>Cabelos</p>
        <a href="<?php echo get_permalink($the_query_cabelos_1[0]->ID); ?>">
          <img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($the_query_cabelos_1[0]->post_title) ?>">
        </a>
        <a href="<?php echo get_permalink($the_query_cabelos_1[0]->ID); ?>">
          <p><?php print_r($the_query_cabelos_1[0]->post_title) ?></p>
        </a>
        <div><?php print_r($the_query_cabelos_1[0]->post_content) ?></div>
        <a href="<?php echo get_permalink($the_query_cabelos_1[0]->ID); ?>">Leia Mais</a>
      </div>
      <?php foreach ($the_query_cabelos as $ret) { ?>
        <div>
          <a href="<?php echo get_permalink($ret->ID); ?>">
            <p><?php print_r($ret->post_title) ?></p>
          </a>
        </div>
      <?php } ?>
    </div>
    <div class="categories-dicas">
      <div>
        <p>Dicas</p>
        <a href="<?php echo get_permalink($the_query_dicas_1[0]->ID); ?>">
          <img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($the_query_dicas_1[0]->post_title) ?>">
        </a>
        <a href="<?php echo get_permalink($the_query_dicas_1[0]->ID); ?>">
          <p><?php print_r($the_query_dicas_1[0]->post_title) ?></p>
        </a>
        <div><?php print_r($the_query_dicas_1[0]->post_content) ?></div>
        <a href="<?php echo get_permalink($the_query_dicas_1[0]->ID); ?>">Leia Mais</a>
      </div>
      <?php foreach ($the_query_dicas as $ret) { ?>
        <div>
          <a href="<?php echo get_permalink($ret->ID); ?>">
            <p><?php print_r($ret->post_title) ?></p>
          </a>
        </div>
      <?php } ?>
    </div>
    <div class="categories-negocios">
      <div>
        <p>Negócios</p>
        <a href="<?php echo get_permalink($the_query_negocios_1[0]->ID); ?>">
          <img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($the_query_negocios_1[0]->post_title) ?>">
        </a>
        <a href="<?php echo get_permalink($the_query_negocios_1[0]->ID); ?>">
          <p><?php print_r($the_query_negocios_1[0]->post_title) ?></p>
        </a>
        <div><?php print_r($the_query_negocios_1[0]->post_content) ?></div>
        <a href="<?php echo get_permalink($the_query_negocios_1[0]->ID); ?>">Leia Mais</a>
      </div>
      <?php foreach ($the_query_negocios as $ret) { ?>
        <div>
          <a href="<?php echo get_permalink($ret->ID); ?>">
            <p><?php print_r($ret->post_title) ?></p>
          </a>
        </div>
      <?php } ?>
    </div>
  </div>

  <a href='#topo'>Voltar ao topo</a>
</div>

<?php
get_footer();
?>