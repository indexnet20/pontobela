<?php
$urlTheme = get_template_directory_uri();

$the_query_lidos =  get_posts(array('posts_per_page'  => 5, 'offset' => 0, 'category' => 11));
?>


<footer>
	<div class="container">
		<div class="posts">
			<p>Post Recentes</p>

			<div>
				<?php $the_query = new WP_Query('posts_per_page=5'); ?>

				<?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
					<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
				<?php
				endwhile;
				?>
			</div>

		</div>
		<div class="most_read">
			<p>Mais Lidas</p>

			<div>
				<?php foreach ($the_query_lidos as $ret) { ?>
					<a href="<?php the_permalink($ret->ID) ?>"><?php print_r($ret->post_title); ?></a>
				<?php } ?>
			</div>
		</div>

		<div class="categories">
			<p>Categorias</p>
			<?php
			wp_nav_menu(array(
				'menu'   => 'Header',
			));
			?>
		</div>

		<div class="logo">
			<a href="/pontobela">
				<img src="<?= get_template_directory_uri(); ?>/img/ponto-bela-completo.svg" alt="">
			</a>
		</div>
	</div>
	<div class="container">
		<p>2022 Ponto Bela - Tudo sobre comésticos e cabelos</p>
		<p>Todos os direitos resevados.</p>
	</div>
</footer>


<script src="<?= get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script src="<?= get_template_directory_uri(); ?>/js/jquery.min.js"></script>
<script src="<?= get_template_directory_uri(); ?>/js/sidebar.js"></script>

</body>

</html>

<?php wp_footer(); ?>