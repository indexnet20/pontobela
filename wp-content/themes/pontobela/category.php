<?php get_header();
$the_query =  get_posts(array('posts_per_page'  => 2, 'offset' => 0,));
$the_query_2 =  get_posts(array('posts_per_page'  => 3, 'offset' => 0,  'category' => 11));
$the_query_3 =  get_posts(array('posts_per_page'  => 3, 'offset' => 3,  'category' => 11));
$the_query_destaques =  get_posts(array('posts_per_page'  => 2, 'offset' => 0, 'category' => 0));



$url = str_replace("Novo/", "", $_SERVER["REQUEST_URI"]);

$split_url = explode('/', $url);

$queried_object = get_queried_object();
?>

<div class="container d-flex w-100 h-100 align-items-center pt-4 flex-column category">
  <div class="content-post">
    <div class="content-post-category">
      <h2><img src="<?= get_template_directory_uri(); ?>/img/monograma-pack-sem-fundo-ponto-bela-bullet.svg" alt="Icon Ponto"><?= $split_url[3]; ?></h2>
      <div class="content-post-category-three">
        <?php
        $not_in = array();

        $the_query_category_three = new WP_Query(array(
          'posts_per_page' => 3,
          'cat' => $queried_object->term_id,
          'paged' => $paged
        ));

        while ($the_query_category_three->have_posts()) : $the_query_category_three->the_post();
          if ($paged == 0) {
            array_push($not_in, get_the_ID());
        ?>
            <div>
              <a href="<?php echo get_permalink(); ?>">
                <img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="">
              </a>
              <a href="<?php echo get_permalink(); ?>">
                <p> <?php the_title(); ?></p>
              </a>

              <div><?php print_r(get_the_content()); ?></div>

              <a href="<?php echo get_permalink(); ?>">Leia Mais</a>
            </div>
          <?php } ?>
        <?php endwhile; ?>
      </div>


      <div>
        <?php
        $post_lista = new WP_Query(array(
          'post__not_in' => $not_in,
          'posts_per_page' => 7,
          'cat' => $queried_object->term_id,
          'paged' => $paged
        ));

        while ($post_lista->have_posts()) : $post_lista->the_post();
        ?>
          <div>
            <a href="<?php echo get_permalink(); ?>">
              <img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="">
            </a>
            <div>
              <a href="<?php echo get_permalink(); ?>">
                <p> <?php the_title(); ?></p>
              </a>

              <div><?php print_r(get_the_content()); ?></div>

              <a href="<?php echo get_permalink(); ?>">Leia Mais</a>
            </div>
          </div>
        <?php endwhile; ?>
      </div>
    </div>
    <div>
      <div class="post-destaques">
        <h2><img src="<?= get_template_directory_uri(); ?>/img/monograma-pack-sem-fundo-ponto-bela-bullet.svg" alt="Icon Ponto">Destaques</h2>
        <?php foreach ($the_query_destaques as $ret) { ?>
          <div>
            <a href="<?php echo get_permalink($ret->ID); ?>">
              <img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($ret->post_title) ?>">

            </a>
            <a href="<?php echo get_permalink($ret->ID); ?>">
              <p><?php print_r($ret->post_title) ?></p>
            </a>
            <div><?php print_r($ret->post_content) ?></div>
          </div>
        <?php } ?>
      </div>
      <div class="post-lidas">
        <h2><img src="<?= get_template_directory_uri(); ?>/img/monograma-pack-sem-fundo-ponto-bela-bullet.svg" alt="Icon Ponto">Mais Lidas</h2>
        <?php foreach ($the_query_2 as $ret) { ?>
          <div>
            <a href="<?php echo get_permalink($ret->ID); ?>">
              <p><?php print_r($ret->post_title) ?></p>
            </a>
            <a href="<?php echo get_permalink($ret->ID); ?>">
              <img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($ret->post_title) ?>">
            </a>
          </div>
        <?php } ?>
      </div>
      <div class="post-propaganda">

      </div>
      <div class="post-lidas">
        <?php foreach ($the_query_3 as $ret) { ?>
          <div>
            <a href="<?php echo get_permalink($ret->ID); ?>">
              <p><?php print_r($ret->post_title) ?></p>
            </a>
            <a href="<?php echo get_permalink($ret->ID); ?>">
              <img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($ret->post_title) ?>">
            </a>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>

  <div class="d-flex paginacao-interna">
    <?php pagination_funtion();  ?>
  </div>

  <div class="d-flex w-100 h-100 align-items-center justify-content-center">
    <a href='#topo'>Voltar ao topo</a>
  </div>
</div>
<?php get_footer(); ?>