<?php get_header();
$the_query =  get_posts(array('posts_per_page'  => 2, 'offset' => 0,));
$the_query_2 =  get_posts(array('posts_per_page'  => 3, 'offset' => 2,));
$the_query_3 =  get_posts(array('posts_per_page'  => 3, 'offset' => 5,));
$the_query_lidos = get_posts(array('posts_per_page'  => 4, 'offset' => 8,));
?>
<div class="container d-flex w-100 h-100 align-items-center pt-4 flex-column search-form">
	<div class="content-post">
		<div>
			<p class="title-search">Resultado de Busca para: <strong><?= $s; ?></strong></p>
			<div>
				<?php
				$e = 0;
				while (have_posts()) : the_post();
				?>
					<div>
						<a href="<?php echo get_permalink(); ?>">
							<img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="">
						</a>
						<div>
							<a href="<?php echo get_permalink(); ?>">
								<p> <?php the_title(); ?></p>
							</a>

							<div><?php print_r(get_the_content()); ?></div>

							<a href="<?php echo get_permalink(); ?>">Leia Mais</a>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
		<div>
			<div class="post-destaques">
				<h2><img src="<?= get_template_directory_uri(); ?>/img/monograma-pack-sem-fundo-ponto-bela-bullet.svg" alt="Icon Ponto">Destaques</h2>
				<?php foreach ($the_query as $ret) { ?>
					<div>
						<a href="<?php echo get_permalink($ret->ID); ?>">
							<img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($ret->post_title) ?>">

						</a>
						<a href="<?php echo get_permalink($ret->ID); ?>">
							<p><?php print_r($ret->post_title) ?></p>
						</a>
						<div><?php print_r($ret->post_content) ?></div>
					</div>
				<?php } ?>
			</div>
			<div class="post-lidas">
				<h2><img src="<?= get_template_directory_uri(); ?>/img/monograma-pack-sem-fundo-ponto-bela-bullet.svg" alt="Icon Ponto">Mais Lidas</h2>
				<?php foreach ($the_query_2 as $ret) { ?>
					<div>
						<a href="<?php echo get_permalink($ret->ID); ?>">
							<p><?php print_r($ret->post_title) ?></p>
						</a>
						<a href="<?php echo get_permalink($ret->ID); ?>">
							<img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($ret->post_title) ?>">
						</a>
					</div>
				<?php } ?>
			</div>
			<div class="post-propaganda">

			</div>
			<div class="post-lidas">
				<?php foreach ($the_query_3 as $ret) { ?>
					<div>
						<a href="<?php echo get_permalink($ret->ID); ?>">
							<p><?php print_r($ret->post_title) ?></p>
						</a>
						<a href="<?php echo get_permalink($ret->ID); ?>">
							<img src="<?php echo get_the_post_thumbnail_url($the_query[0]->ID); ?>" alt="<?php print_r($ret->post_title) ?>">
						</a>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<div class="d-flex paginacao-interna">
		<?php pagination_funtion();  ?>
	</div>
</div>
<?php get_footer(); ?>