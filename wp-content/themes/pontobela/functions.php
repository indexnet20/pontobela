<?php
add_theme_support('title-tag');
add_theme_support('post-thumbnails');
add_theme_support('menus');

add_filter('auto_update_plugin', '__return_true');

include_once('descricao-arquivo.php');


// Chama arquivos JS

// Descrição do Menu
function index_nav_description($item_output, $item, $depth, $args)
{
  if ('primary' == $args->theme_location && $item->description) {
    $item_output = str_replace($args->link_after . '</a>', '<div class="menu-item-description">' . $item->description . '</div>' . $args->link_after . '</a>', $item_output);
  }

  return $item_output;
}
add_filter('walker_nav_menu_start_el', 'index_nav_description', 10, 4);

add_action('admin_enqueue_scripts', function () {
  /*
    if possible try not to queue this all over the admin by adding your settings GET page val into next
    if( empty( $_GET['page'] ) || "my-settings-page" !== $_GET['page'] ) { return; }
    */
  wp_enqueue_media();
});


// File Name
add_filter('sanitize_file_name', 'index_sanitize_file_name', 10);
function index_sanitize_file_name($filename)
{
  return strtolower(remove_accents($filename));
}


// Desativa o menu Comentários do Painel Administrativo
function cwp_desativa_comentarios_admin_menu()
{
  remove_menu_page('edit-comments.php');
}
add_action('admin_menu', 'cwp_desativa_comentarios_admin_menu');


require get_template_directory() . '/inc/custom-header.php';


require get_template_directory() . '/inc/template-tags.php';

require get_template_directory() . '/inc/customizer.php';



// Limite de caracteres
function excerpt($limit)
{
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt) >= $limit) {
    array_pop($excerpt);
    $excerpt = implode(" ", $excerpt) . '...';
  } else {
    $excerpt = implode(" ", $excerpt);
  }
  $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
  return $excerpt;
}



function wordpress_pagination()
{
  global $wp_query;
  $big = 999999999;
  echo paginate_links(array(
    'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
    'format' => '?paged=%#%',
    'current' => max(1, get_query_var('paged')),
    'total' => $wp_query->max_num_pages
  ));
}


function new_excerpt_more($more)
{
  return '';
}
add_filter('excerpt_more', 'new_excerpt_more');



/** Pagination */
function pagination_funtion()
{
  // Get total number of pages
  global $wp_query;
  $total = $wp_query->max_num_pages;
  // Only paginate if we have more than one page                   
  if ($total > 1) {
    // Get the current page
    if (!$current_page = get_query_var('paged'))
      $current_page = 1;

    $big = 999999999;
    // Structure of "format" depends on whether we’re using pretty permalinks
    $permalink_structure = get_option('permalink_structure');
    $format = empty($permalink_structure) ? '&page=%#%' : 'page/%#%/';
    echo paginate_links(array(
      'base' => str_replace($big, '%#%', get_pagenum_link($big)),
      'format' => $format,
      'current' => $current_page,
      'total' => $total,
      'mid_size' => 2,
      'type' => 'list',
      'prev_text' => __('<'),
      'next_text' => __('>'),
    ));
  }
}
/** END Pagination */


remove_action('wp_body_open', 'wp_global_styles_render_svg_filters');
remove_action('render_block', 'wp_render_duotone_support', 10);
remove_action('wp_enqueue_scripts', 'wp_enqueue_global_styles');
remove_action('wp_footer', 'wp_enqueue_global_styles', 1);


add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar()
{
  if (!current_user_can('administrator') && !is_admin()) {
    show_admin_bar(false);
  }
}
