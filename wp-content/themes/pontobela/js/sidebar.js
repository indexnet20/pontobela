function openSidebar(){
  $('sidebar').toggleClass('active');
  $('.overlay').toggleClass('active');
}

$(document).ready(function () { 
  $('.overlay').on('click', function () {
    $('.overlay').toggleClass('active');
    $('sidebar').toggleClass('active');
  });  
});